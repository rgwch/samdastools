const { gunzip, gzip } = require('zlib')
const { promisify } = require('util')
const unzip = promisify(gunzip)
const zip = promisify(gzip)


exports.compress = async (data) => {
    const inp = Buffer.from(data, "utf-8")
    const outp = await zip(inp)
    const ret = Buffer.allocUnsafe(outp.length + 4)
    outp.copy(ret, 4, 0)
    return ret
}

exports.decompress = async data => {
    const inp = Buffer.from(data)
    const prep = Buffer.allocUnsafe(inp.length - 4)
    inp.copy(prep, 0, 4)
    const ret = await unzip(prep)
    return ret.toString("utf-8")
}

