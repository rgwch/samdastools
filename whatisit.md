# Samdas tools

Samdas ist ein XML-Format, welches zur Darstellung von Konsultationstexten in Elexis entworfen wurde. Das Ziel war, Attribute, die im damals aktuellen HTML 4 nicht darstellbar waren, einzubinden (Beispielsweise Elemente, die bei Doppelklick mit einer externen Anwendung oder einem spezifischen Plugin, etwa dem Elexis Text Plugin, geöffnet werden konnten.)

Die Originalimlementation findet sich in `ch.elexis.core:ch.elexis.core.text.model.Samdas.java` und die Anwendung in  `ch.elexis.core.ui:ch.elexis.core.ui.text/EnhancedTextField.java`

Samdas trennt den Text strikt von Metainformationen: Ein Element ('text') enthält nur den Plaintext, andere Elemente enthalten Markups ('markup') oder eben Querverweise zu externen Anwendungen oder Plugins ('xref'). Der Bezug zum Text wird über Positions- und Längenattribute hergestellt.

Während dieses Konzept aus theoretischer Sicht gut ist (Suchen, indizieren etc. wird einfacher, wenn man den Plaintext benutzen kann), zeigten sich doch Probleme durch Desynchronisation von Text und Markups, wenn Anwender Text löschten und einfügten. 

Heute mit HTML5 steht eine bessere Möglichkeiten bereit, custom content unterzubringen: Die data-* Attribute. Da HTML5 ein weit verbreitetes Standardformat ist, würde sich ein Wechsel eigentlich aufdrängen. Allerdings existiert eine grosse Datenbasis existierender Elexis-Konsultationseinträge, so dass Kompatibilität gewahrt bleiben muss.

## Problemstellung

Für [Webelexis](http://www.webelexis.ch) musste eine Möglichkeit gefunden werden, Konsultationseinträge anzuzeigen und zu speichern.

Elexis-Konsultationseintäge existieren bereits in drei verschiedenen Varianten, je nach Alter der Installation:

* Plaintext. Die ersten Versionen bis 1.4 speicherten Nur den Textinhalt. Manche Zeichen hatten spezielle Bedeutung für die Darstellung: *fett*, /kursiv/, _unterstrichen_ und Zeilenanfang: 

* Samdas: Ab Elexis Version 1.4 eingeführt. Prinzip:

````
<samdas>
  <text>Dies ist Text und ein [Link]</text>
  <markup from="9" length="5" type="bold">
  <xref from="22" length="6" id="93042wer4534t35" provider="ch.elexis.text.DocXRef">
</samdas>
````

* SimpleStructuredDocument. Wurde für kurze Zeit als Ersatz für Samdas eingesetzt, um einige von dessen Beschränkungen zu umgehen. Das betrifft nur einige wenige Installationen, und mit Elexis V2.1 wurde wieder komplett auf Samdas umgestellt.

Die drei Eintragstypen müssen unterschieden und korrekt behandelt werden. Elexis selbst macht das so, dass Einträge mit einer &lt; am Anfang als Samdas behandelt werden, und SST-fähige Elexisversionen auserdem den Tag <SimpleStructuredText> auswerten.

Leider gibt es keine standardisierte Definition von Zeilenumbrüchen. Je nach Elexis-Version, Betriebssystem und Java-Version existiert eine bunte Mischung aus "\n", "\r", "\r\n" und "#0xD;". Das führt dazu, dass die "from"- Attribute der Markups und Xrefs nur dann stabil stimmen, wenn man das XML im org.eclipse.swt.custom.StyledText - Widget interpretiert, welches die Unterschiede ausbügelt. Elexis macht das in ch.elexis.core.ui:ch.elexis.core.ui.text.EnhancedTextField.


Glücklicherweise lassen sich (die meisten) Markups und Xrefs auch anders finden: Xrefs sind meist in [eckige Klammern] gesetzt und Markups sind als \*bold\*, \/kursiv\/ etc. ausgezeichnet.

Ein grösseres Problem ist die Rückverwandlung: HTML hat viel mehr Darstellungsmöglichkeiten, als Samdas. Samdas kennt weder unterschiedliche Schriftgrössen, noch Schriftarten, noch Farben. Und schlimmer noch: Die existierenden Samdas-Implementationen entfernen Markups, die sie nicht verstehen, anstatt sie zu ignorieren.

## Realisierung

