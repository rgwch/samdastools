/*********************************************
 * Conversion tool from and to Elexis-Samdas *
 * Copyright (c) 2016-2018 by G. Weirich     *
 * License and Terms see LICENSE             *
 *********************************************/

const builderOpts = {
  rootName: "samdas:EMR",
  xmldec: { standalone: undefined, encoding: "UTF-8" },
  renderOpts: { pretty: true }
}
const xmlbuilder = new (require('xml2js')).Builder(builderOpts);

const Delta = require('quill-delta')

exports.fromDelta = (deltaText) => {
  if (!deltaText || !Array.isArray(deltaText.ops)) {
    throw new Error("bad parameter " + deltaText)
  }
  const delta = new Delta(deltaText.ops)
  let text = ""
  let pos = 0
  let markups = []
  let xrefs = []
  addMarkup = (txt, typ) => {
    markups.push({ $: { type: typ, from: pos, length: txt.length } })
    text += txt
    pos = text.length
  }
  delta.forEach(op => {
    if (!op.insert) {
      debugger
    }
    if (op.attributes) {
      if (op.attributes.bold) {
        addMarkup(op.insert, "bold")
      }
      if (op.attributes.italic) {
        addMarkup(op.insert, "italic")
      }
      if (op.attributes.underline) {
        addMarkup(op.insert, "underline")
      }
      if (op.attributes.xref) {
        xrefs.push({
          $: {
            from: pos,
            length: op.insert ? op.insert.length : 0,
            provider: op.attributes.xrefProvider,
            id: op.attributes.xrefId
          }
        })
        text += op.insert
        pos = text.length
      }
    } else {
      text += op.insert
      pos = text.length
    }
  })
  if (text.endsWith('\n')) {
    text = text.substr(0, text.length - 1)
  }
  const samdas = {
    $: {
      "xmlns:samdas": "http://www.elexis.ch/XSD"
    },
    "samdas:record": {
      "samdas:text": text,
      "samdas:markup": markups,
      "samdas:xref": xrefs
    }
  }
  return xmlbuilder.buildObject(samdas)
}

/**
 * Remove all markup from a text and convert line breaks to  \n
 * @param text
 * @returns {string|XML}
 */
const cleanHTML = function (text) {
  var ret = text.replace(/<br ?\/>/g, "\n");
  return ret.replace(/<.+?>/g, "")
};


exports.fromHtml = (htmlText) => {
  const parser = require('parse5')
  const ast = parser.parseFragment(htmlText)
  let text = ""
  const markups = []
  const xrefs = []
  const hints = []
  function addNode(node, def) {
    if (node.nodeName === "#text") {
      const insert = node.value.replace(/\n\s*$/g, "\n").replace(/\n{3,}/g,"\n\n")
      text += insert
      if (def) {
        def.length = insert.length
        switch (def.type) {
          case "italic":
          case "bold":
          case "underline":
            delete def.linebreak;
            markups.push({ $: def })
            break;
          case "h1":
          case "h2":
          case "h3":
          case "color":
            hints.push({ $: def })
            break;
          case "xref":
            delete def.type
            delete def.linebreak;
            xrefs.push({ $: def })
            break;
          case "p":
            delete def.type
            break;
          default:
            if (def.type) {
              log.warning("Builder: unknown markup " + JSON.stringify(def))
            }
        }
      }
    } else {
      const def = {
        from: text.length,
        linebreak: false
      }

      switch (node.tagName) {
        case "h1": def.type = "h1"; def.linebreak = true; break;
        case "h2": def.type = "h2"; def.linebreak = true; break;
        case "h3": def.type = "h3"; def.linebreak = true; break;
        case "i": def.type = "italic"; break;
        case "b":
        case "strong": def.type = "bold"; break;
        case "br":
        case "p": def.type = "p", def.linebreak = true;
      }
      if (def.type == undefined) {
        if (node.attrs) {
          for (let attr of node.attrs) {
            if (attr.name === "style") {
              if (attr.value.match(/.+underline;?/)) {
                def.type = "underline";
              }
              if (attr.value.match(/font.+bold;?/)) {
                def.type = "bold"
              }
            } else if (attr.name === "data-xref-id") {
              def.type = "xref"
              def.id = attr.value
            } else if (attr.name === "data-xref-provider") {
              def.type = "xref"
              def.provider = attr.value
            }
          }
        }
      }
      for (let child of node.childNodes) {
        addNode(child, def)
      }
      if (def.linebreak) {
        text += "\n"
      }
    }
  }
  for (let child of ast.childNodes) {
    addNode(child)
  }

  const samdas = {
    $: {
      "xmlns:samdas": "http://www.elexis.ch/XSD"
    },
    "samdas:record": {
      "samdas:text": text,
      "samdas:markup": markups,
      "samdas:xref": xrefs,
      "samdas:hints": hints
    }
  }
  return xmlbuilder.buildObject(samdas)

}

