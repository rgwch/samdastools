# Samdastools

This is only useful for development around the [Elexis](http://www.elexis.info)(tm) electronic medical record system. 

It converts between the Samdas format as used in Elexis' encounter entries, and standard formats [HTML](https://html.spec.whatwg.org/multipage/) and [Delta](https://quilljs.com/docs/delta/).

## install

    npm install --save @rgwch/samdastools

## Usage

````
const Samdas=require('@rgwch/samdastools')

const deltaText=Samdas.toDelta(samdastext)
const htmlText=Samdas.toHtml(samdastext)
const samdas1=Samdas.fromHtml(htmltext)
const samdas2=Samdas.fromDelta(deltaText)
````

## Test

    jasmine

## License

MIT
