/*********************************************
 * Conversion tool from and to Elexis-Samdas *
 * Copyright (c) 2016-2018 by G. Weirich     *
 * License and Terms see LICENSE             *
 *********************************************/

const parse = require('xml-parser')
const Delta = require('quill-delta')
const check = require('./check').repair


exports.plaintextToDelta = plaintext => {
  //const outlinePattern = /"^\S+:/gm
  //const boldPattern = /\*\S+\*/g 
  //const italicPattern = /\/\S+\//g
  //const underlinePattern = /_\S+_/g
  const pat = /^\S+?:|\*\S+?\*|\/\S+?\/|_\S+?_/gm
  let match
  let start = 0
  let end = 0
  let delta = new Delta()
  while ((match = pat.exec(plaintext)) !== null) {
    const idx = match.index
    const fragment = match[0]
    end = pat.lastIndex
    if (match.index > start) {
      delta.insert(plaintext.substring(start, match.index))
    }
    const attributes = {}
    if (fragment.startsWith("*") || fragment.endsWith(":")) {
      attributes.bold = true
    } else if (fragment.startsWith("_")) {
      attributes.underline = true
    } else if (fragment.startsWith("/")) {
      attributes.italic = true
    } else {
      attributes[fragment.substring(0, 1)] = true
    }
    delta.insert(fragment, attributes)
    start = end
  }
  if (start < plaintext.length) {
    delta.insert(plaintext.substring(start))
  }
  delta.insert("\n")
  return delta
}

exports.plaintextToHtml = async plaintext => {
  const pattern = /^\S+?:|\*\S+?\*|\/\S+?\/|_\S+?_/gm
  let match
  let start = 0
  let end = 0
  let dest=""
  while ((match = pattern.exec(plaintext)) !== null) {
    const idx = match.index
    const fragment = match[0]
    // end = pat.lastIndex
    if (match.index > start) {
      dest+=(plaintext.substring(start, match.index))
    }
    const attributes = {}
    if (fragment.startsWith("*") || fragment.endsWith(":")) {
      dest+=`<b>${fragment}</b>`
    } else if (fragment.startsWith("_")) {
      dest+=`<span style="text-decoration:underline">${fragment}</span>`
    } else if (fragment.startsWith("/")) {
      dest+=`<i>${fragment}</i>`
    } else {
     dest+=fragment
    }
    start = end
  }
  if (start < plaintext.length) {
    dest+=(plaintext.substring(start))
  }
  
  dest= dest.replace(/[\n\r][\r\n]?/g,"<br />")
  return dest.replace(/(<br\s*\/>{3,})/,"<br />")
}

exports.samdasToHtml = async samdastext => {
  const raw = await check(samdastext) //samdastext.replace(/&#xD;/g, "\n")
  const analyze = parse(raw.replace(/\n/g, "&#xD;"))
  const record = analyze.root.children[0]
  const elements = record.children
  const texts = elements.filter(element => {
    return (element.name.indexOf('text') > -1)
  }).map(e => e.content)
  const xrefs = elements.filter(element => {
    return (element.name.indexOf('xref') > -1)
  }).map(e => {
    e.attributes.type = "xref"
    return e.attributes
  })
  const hints = elements.filter(element => {
    return (element.name.indexOf('hint') > -1)
  }).map(e => e.attributes)
  const markups = elements.filter(element => {
    return (element.name.indexOf('markup') > -1)
  }).map(e => e.attributes)
    .concat(xrefs)
    .concat(hints)
    .sort((a, b) => a.from - b.from)
  let plaintext = ""
  texts.forEach(text => { plaintext = plaintext + text })
  plaintext = plaintext.replace(/&#xD;/g, "\n")
  if (plaintext === "undefined") {
    plaintext = ""
  }
  let dest = ""
  let pos = 0
  for (let markup of markups) {
    let marked = plaintext.substr(markup.from, markup.length)
    dest += plaintext.substring(pos, markup.from)
    switch (markup.type) {
      case "bold": dest += `<strong>${marked}</strong>`; break;
      case "italic": dest += `<i>${marked}</i>`; break;
      case "underline":
        dest += `<span style="text-decoration:underline">${marked}</span>`;
        break;
      case "xref":
        dest += `<span data-xref-id="${markup.id}" data-xref-provider="${markup.provider}">${marked}</span>`
        break;
      case "h1": dest += `<h1>${marked}</h1>`; break;
      case "h2": dest += `<h2>${marked}</h2>`; break;
      case "h3": dest += `<h3>${marked}</h3>`; break;
      case "h4": dest += `<h4>${marked}</h4>`; break;
      default:
        dest += `<span style="color:red">unknown markup</span>`
    }

    pos = parseInt(markup.from) + parseInt(markup.length)

    if (markup.linebreak && plaintext.charAt(pos) == "\n") {
      pos += 1
    }
  }
  if(pos<plaintext.length){
    dest+=plaintext.substr(pos)
  }
  dest=dest.replace(/\n\n/g,"\n")
  return dest.replace(/\n/g, "<br />")
}

exports.samdasToDelta = async samdastext => {
  try {
    const raw = await check(samdastext) //samdastext.replace(/&#xD;/g, "\n")
    const analyze = parse(raw.replace(/\n/g, "&#xD;"))
    const record = analyze.root.children[0]
    const elements = record.children
    const texts = elements.filter(element => {
      return (element.name.indexOf('text') > -1)
    }).map(e => e.content)
    const xrefs = elements.filter(element => {
      return (element.name.indexOf('xref') > -1)
    }).map(e => e.attributes)
    const markups = elements.filter(element => {
      return (element.name.indexOf('markup') > -1)
    }).map(e => e.attributes).concat(xrefs).sort((a, b) => a.from - b.from)
    let plaintext = ""
    texts.forEach(text => { plaintext = plaintext + text })
    plaintext = plaintext.replace(/&#xD;/g, "\n") //.replace(/\n\n/g,'\n')
    if (plaintext === "undefined") {
      plaintext = ""
    }
    let delta = new Delta([{ insert: plaintext }])
    markups.forEach(markup => {
      const attribute = markup.type ? { [markup.type]: true } : { xref: true, xrefId: markup.id, xrefProvider: markup.provider }
      const item = new Delta().retain(parseInt(markup.from)).retain(parseInt(markup.length), attribute)
      delta = delta.compose(item)
    })
    
    delta.ops.forEach(op=>{
      if(op.insert=="\n\n"){
        op.insert="\n"
      }
    })
    
    delta.insert("\n")
    // console.log(inspect(delta, { colors: true, depth: Infinity }))
    return delta
  } catch (err) {
    return new Delta([{ "insert": samdastext }])
  }
}

exports.deltaToHtml = (deltaText) => {
  const ops = deltaText.ops
  if (!ops) {
    return ""
  }
  if (!Array.isArray(ops)) {
    throw new Error("Bad parameter type")
  }
  let ret = ""
  for (let op of ops) {
    const attr = op.attributes
    if (op.insert) {
      if (op.insert.match(/^\n+$/g)) {
        ret += op.insert.replace(/\n/g, "<br />")
      } else {
        const text = op.insert.replace(/\n/g, "<br />")
        if (attr) {
          if (attr.bold) {
            ret += `<span style="font-weight:bold;">${text}</span>`
          } else if (attr.italic) {
            ret += `<span style="font-style:italic;">${text}</span>`
          } else if (attr.underline) {
            ret += `<span style="text-decoration:underline;">${text}</span>`
          } else if (attr.xref) {
            ret += `<span style="font-style:normal;" data-xref-id="${attr.xrefId}" data-xref-provider="${attr.xrefProvider}">${text}</span>`
          } else {
            ret += `<span data-attr="${JSON.stringify(attr)}">${text}</span>`
          }
        } else {
          ret += `<span style="font-style:normal;">${text}</span>`
        }
      }
    }
  }
  return ret
}

function scan(e, attrib) {
  let ret = new Delta()
  if (e.node == "text") {
    ret.insert(e.text, attrib)
  } else if (e.node == "element") {
    const attribute = {}
    if (e.attr) {
      if (e.attr.style) {
        if (e.attr.style.match(/font.+italic;?/)) {
          attribute.italic = true
        } else if (e.attr.style.match(/font.+bold;?/)) {
          attribute.bold = true
        } else if (e.attr.style.match(/text.+underline;?/)) {
          attribute.underline = true
        }
      }
      if (e.attr["data-xref-id"]) {
        attribute.xrefId = e.attr["data-xref-id"]
        attribute.xrefProvider = e.attr["data-xref-provider"]
        attribute.xref = true
      }
    }
    if (e.tag) {
      if (e.tag == "i") {
        attribute.italic = true
      } else if (e.tag == "strong") {
        attribute.bold = true
      } else if (e.tag == "br") {
        ret.insert('\n', attribute)
      } else {
        debugger
      }
    }
    if (e.child) {
      for (let child of e.child) {
        const other = scan(child, attribute)
        ret = ret.concat(other)
      }
    }
  }
  return ret
}
exports.htmlToDelta = htmlText => {
  const html2json = require('html2json').html2json
  const stripped = htmlText.replace(/\n/g, "").replace(/&nbsp;/g, " ").replace(/(<br *\/?>)+$/g, "\n")

  const json = html2json(stripped)
  let delta = new Delta()
  for (let child of json.child) {
    delta = delta.concat(scan(child, {}))
  }
  return delta
}