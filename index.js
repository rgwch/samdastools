/*********************************************
 * Conversion tool from and to Elexis-Samdas *
 * Copyright (c) 2016-2018 by G. Weirich     *
 * License and Terms see LICENSE             *
 *********************************************/

const inspect = require('util').inspect
const SamdasParser = require('./parser')
const debug = require('debug')("Samdastools")
const logger=require('./logger')
const zipper=require('./zipper')

/** 
 * Convert a Samdas Document to Quill-Delta
 */
toDelta = async samdastext => {
  try {
    if (samdastext.trim().startsWith("<")) {
      return await SamdasParser.samdasToDelta(samdastext)
    } else {
      return SamdasParser.plaintextToDelta(samdastext)
    }
  } catch (err) {
    return samdastext
  }
}

zippedToDelta = async samdasZipped =>{
  const unzipped=await zipper.decompress(samdasZipped)
  const delta=await toDelta(unzipped)
  return delta
}
/**
 * Convert a Samdas Document to HTML
 */
toHtml = async samdastext => {
  try {
    if (samdastext.trim().startsWith("<")) {
      return await SamdasParser.samdasToHtml(samdastext)
    } else {
      return SamdasParser.plaintextToHtml(samdastext)
    }
  } catch (err) {
      return samdastext
  }
}

/**
 * Convert a HTML Document to Samdas
 */
fromHtml = htmlText => {
  return require('./builder').fromHtml(htmlText)
}

/**
 * Convert a Quill-Delta Document to Samdas
 */
fromDelta = deltaText => {
  return require('./builder').fromDelta(deltaText)
}

zippedFromDelta = async deltaText =>{
  const delta=fromDelta(deltaText)
  const zipped=await zipper.compress(delta)
  return zipped
}
module.exports = {
  toDelta, toHtml, fromHtml, fromDelta, zippedFromDelta, zippedToDelta
}