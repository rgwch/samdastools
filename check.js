/*********************************************
 * Conversion tool from and to Elexis-Samdas *
 * Copyright (c) 2016-2018 by G. Weirich     *
 * License and Terms see LICENSE             *
 *********************************************/
const log = require('./logger')
log.level='warning'

const builderOpts = {
  rootName: "samdas:EMR",
  xmldec: { standalone: undefined, encoding: "UTF-8" },
  renderOpts: { pretty: false }
}

const xmljs = require('xml2js')
const xmlbuilder = new xmljs.Builder(builderOpts)
const Parser = new xmljs.Parser()
let text
let markups = []
let xrefs = []

/**
 * Test if the current markup is one of *bold*, /italic/, _underline_, [xref], or
 * ^linestart:
 * 
 * @param {} scanpos Position of first char (except for linestart, see below)
 * @param {*} length Length of markup
 */
function isSymbol(scanpos, length) {
  const endpos = scanpos + length - 1
  // Test for a word between special chars *,/,_
  if (text.charAt(scanpos).match(/[\*\/_]/)) {
    if (text.charAt(scanpos) === text.charAt(endpos)) {
      log.debug("found symbol: %s", text.substr(scanpos, length))
      return [true, 0]
    }

    /* test for a string in square brackets. Note: The Elexis Samdas implementation
    does not set the length consistently correct */
  } else if (text.charAt(scanpos) == "[") {
    if (text.charAt(endpos) == "]") {
      return [true, 0]
    }
    if (text.charAt(endpos - 1) == "]") {
      log.debug("found bracket: %s", text.substr(scanpos, length))
      return [true, -1]
    }


    /* Test for a word starting at the beginning of a line and 
      ending with :. Note: The Elexis Samdas implementation has a bug here:
      The newline before the word is counted for the match */
  } else if ((scanpos > 0) && text.substr(scanpos - 1, length + 1).match(/\n\S+:/m)) {
    log.debug("found line start: %s", text.substr(scanpos, length))
    return [true, 0]
  }
  return [false, 0]
}
function isWordBound(scanpos, length) {
  log.debug("checking word bounds at position %d, string %s", scanpos, text.substr(scanpos, length))
  if (scanpos == 0) {
    if (text.substr(scanpos, length + 1).match(/^\S+\s?$/m)) {
      log.debug("found word at line start %s", text.substr(scanpos, length))
      return true
    }
  } else {
    if (text.substr(scanpos-1, length + 2).match(/[\.,\?!\"\'\s\n]\S+\s?$/m)) {
      log.debug("found word bounding: %s", text.substr(scanpos, length))
      return true
    }
    
  }
 
  return false
}
function scan(pos, length) {
  const checkpositions = [0, -1, 1, -2, 2, -3, 3, -4, 4, -5, 5]
  for (let offset of checkpositions) {
    const scanpos = pos + offset
    log.debug("checking markers at position %d, length %d, string %s", scanpos, length, text.substr(scanpos, length))
    const symbolResult = isSymbol(pos + offset, length)
    if (symbolResult[0] == true) {
      return [scanpos, length + symbolResult[1]]
    }
  }
  for (let offset of checkpositions) {
    const scanpos = pos + offset
    if (isWordBound(scanpos + offset, length)) {
      return [scanpos, length]
    }
  }
  log.debug("no match found")
  return [pos, length]
}

function checkAlignment(m) {
  const ret = Object.assign({}, m)
  log.debug("checking: " + JSON.stringify(ret))
  const adjust = scan(parseInt(ret.$.from), parseInt(ret.$.length))
  ret.$.from = adjust[0]
  ret.$.length = adjust[1]
  return ret
}
exports.repair = samdastext => {
  text = ""
  markups = []
  xrefs = []
  hints = []
  return new Promise((resolve, reject) => {
    Parser.parseString(samdastext, (err, orig) => {
      if (err) {
        reject(err)
      }
      let root = orig["samdas:EMR"]
      if (!root) {
        root = orig.samdas
      }
      if (!root) {
        reject("no valid root element found")
      }
      let record = root["samdas:record"]
      if (!record) {
        record = root.record
      }
      if (!record || !Array.isArray(record) || record.length != 1) {
        reject("bad Samdas format: No valid record entry")
      }
      const rec = record[0]
      let tx = rec["samdas:text"]
      if (!tx) {
        tx = rec.text
      }
      text = tx[0].replace(/&#xD;/g, "\n")
      let lm = rec["samdas:markup"]
      if (!lm) {
        lm = rec.markup
      }
      if (lm && Array.isArray(lm)) {
        for (markup of lm) {
          markups.push(checkAlignment(markup))
        }
      }
      let xr = rec["samdas:xref"]
      if (!xr) {
        xr = rec.xref
      }
      if (xr && Array.isArray(xr)) {
        for (xref of xr) {
          xrefs.push(checkAlignment(xref))
        }
      }
      let extensions = rec["samdas:hints"]
      if (!extensions) {
        extensions = rec.hints
      }
      if (extensions && Array.isArray(extensions)) {
        for (ext of extensions) {
          hints.push(checkAlignment(ext))
        }
      }
      const ret = {
        $: {
          "xmlns:samdas": "http://www.elexis.ch/XSD"
        },
        "samdas:record": {
          "samdas:text": text,
          "samdas:markup": markups,
          "samdas:xref": xrefs,
          "samdas:hints": hints
        }
      }
      resolve(xmlbuilder.buildObject(ret))
    })
  })

}