/*********************************************
 * Conversion tool from and to Elexis-Samdas *
 * Copyright (c) 2016-2018 by G. Weirich     *
 * License and Terms see LICENSE             *
 *********************************************/

const fs = require('fs')
const path = require('path')
const Samdas = require('../index')
const Delta = require('quill-delta')
const parse = require('xml-parser')
const inspect = require('util').inspect
const check = require('../check').repair
const zipper=require('../zipper')

describe("Samdas", () => {
  let dir
  beforeAll(() => {

    dir = fs.readdirSync(path.join(__dirname, "rsc", "konsen"))
  })
  xit("handles zipped samdas buffers", async () => {
    const samdastext = fs.readFileSync(path.join(__dirname, 'rsc', 'samdas1.xml'), 'utf8')
    const compressed=await zipper.compress(samdastext)
    const delta=await Samdas.zippedToDelta(compressed)
    const restored=await Samdas.zippedFromDelta(delta)
    const compare=await zipper.decompress(restored)
    expect(compare).toEqual(samdastext)
    expect(restored).toEqual(compressed)
  })
  
  it("converts samdas to Delta", async () => {
    const samdastext = fs.readFileSync(path.join(__dirname, 'rsc', 'samdas1.xml'), 'utf8')
    const deltatext = fs.readFileSync(path.join(__dirname, 'rsc', 'samdas1.delta'), 'utf8')
    const delta = await Samdas.toDelta(samdastext)
    deltaOrig = new Delta(JSON.parse(deltatext).ops)
    /*console.log(deltaOrig)
    console.log("--------")
    console.log(delta)*/
    expect(delta).toEqual(deltaOrig)
  })
  xit("converts samdas to html", async () => {
    const samdastext = fs.readFileSync(path.join(__dirname, 'rsc', 'samdas1.xml'), 'utf8')
    const htmltext = fs.readFileSync(path.join(__dirname, 'rsc', 'samdas1.html'), 'utf8')
    const html = await Samdas.toHtml(samdastext)
    const orig = htmltext.replace(/>[\s+]</g, "><")
    /*console.log("\n-------\n")
    console.log(html)
    console.log("----------")
    console.log(orig)*/
    expect(html).toEqual(orig)
  })
  it("converts Delta to samdas", () => {
    const deltatext = fs.readFileSync(path.join(__dirname, 'rsc', 'samdas1.delta'), 'utf8')
    const samdastext = fs.readFileSync(path.join(__dirname, 'rsc', 'samdas1.xml'), 'utf8')
    const generatedSamdas = Samdas.fromDelta(JSON.parse(deltatext))
    const testee = parse(generatedSamdas)
    const bench = parse(samdastext)
    // const a=testee.root.children[0].children[0].content
    //const b=bench.root.children[0].children[0].content
    //expect(a.substring(120,140)).toEqual(b.substring(120,140))
    expect(testee).toEqual(bench)
  })
  xit("converts html to samdas", () => {
    const htmltext = fs.readFileSync(path.join(__dirname, 'rsc', 'samdas1.html'), 'utf8')
    const samdastext = fs.readFileSync(path.join(__dirname, 'rsc', 'samdas1.xml'), 'utf8')
    const generatedSamdas = Samdas.fromHtml(htmltext)
    /*
    console.log(samdastext)
    console.log("\n-----------\n")
    console.log(generatedSamdas)
    */
    const testee = parse(generatedSamdas)
    const bench = parse(samdastext)
    expect(testee).toEqual(bench)
  })
  it("converts plaintext to Delta", async () => {
    const plaintext = fs.readFileSync(path.join(__dirname, 'rsc', 'samdas2.txt'), 'utf8')
    delta2 = fs.readFileSync(path.join(__dirname, 'rsc', 'samdas2.delta'), 'utf8')

    const delta = await Samdas.toDelta(plaintext)
    const deltaOrig = new Delta(JSON.parse(delta2).ops)
    expect(delta).toEqual(deltaOrig)
  })

  xit("converts plaintext to html", async () => {
    const plaintext = fs.readFileSync(path.join(__dirname, 'rsc', 'samdas2.txt'), 'utf8')
    const html = fs.readFileSync(path.join(__dirname, 'rsc', 'samdas2.html'), 'utf8')
    const converted = await Samdas.toHtml(plaintext)
    expect(converted).toEqual(html)
  })
  xit("batch converts all samdas to delta and html", async () => {
    for (let file of dir) {
      console.log(file)
      const samdasfrom = fs.readFileSync(path.join(__dirname, 'rsc', 'konsen', file), 'utf8')
      if (file == "2007011x.xml") {
        debugger
      }
      const delta = await Samdas.toDelta(samdasfrom)
      if (delta.ops.some(op => op.retain)) {
        debugger
      }
      const samdasfinal = Samdas.fromDelta(delta)

      if (samdasfrom.trim().startsWith("<")) {
        const corrected = await check(samdasfrom.replace(/&#xD/g, "\n"))
        console.log(corrected)
        console.log("-------------")
        console.log(samdasfinal)
        const testee = parse(samdasfinal)
        const bench = parse(corrected)
        expect(testee).toEqual(bench)
      }
    }
  })

  it("converts directly from html to samdas and back to html", async () => {
    const htmltext = `<h1>Titel</h1>Lorem ipsum <b>dolor</b> sit amet.<h2>Untertitel</h2>Loretto <i>ipsum</i> latissimus <strong>dolarat</strong><br/>Nunc <span style="text-decoration:underline">est bibendum!</span><br /><span style="font-style:normal;" data-xref-id="dfece5f44b6060b7740512" data-xref-provider="ch.elexis.text.DocXRef">[ 11.09.2018 Rezept 11.09.2018 ]</span><br /><span style="text-decoration: underline">unterstrichen</span> <span style="font-weight:bold">bold</span>`
    const samdastext = require('../builder').fromHtml(htmltext)
    const checked = await check(samdastext)
    /*
    console.log(samdastext)
    console.log("-------------")
    console.log(checked)  */
    const testee = parse(samdastext)
    const bench = parse(checked)
    function checkWords(el) {
      const t = el[0].content
      const words = ["dolor", "ipsum", "dolarat", "est bibendum!", "unterstrichen", "bold",
        "[ 11.09.2018 Rezept 11.09.2018 ]", "Titel", "Untertitel"]
      for (let i = 0; i < words.length; i++) {
        const tt = t.substr(el[i + 1].attributes.from, el[i + 1].attributes.length)
        expect(tt).toEqual(words[i])
      }

    }
    checkWords(testee.root.children[0].children)
    checkWords(bench.root.children[0].children)
    expect(testee).toEqual(bench)
    const reconverted = await (require('../parser').samdasToHtml(samdastext))
    /*
    console.log(htmltext)
    console.log("-------------")
    console.log(reconverted)
    expect(reconverted).toEqual(samdastext)
    */
  })

})